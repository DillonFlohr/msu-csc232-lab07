/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Dillon Flohr <dillon987@live.missouristate.edu> -Driver
 *          Taylor Kuttenkuler <tay124@live.missouristate.edu> -Co-Navigator
 *			Luba Sidlinskaya <sidlinskaya1@live.missouristate.edu> -Navigator
 * @brief   Implementation file for Lab 7.
 *
 */
#include <iostream>
#include <string>
std::string testIsWord(std::string);
std::string testStrIsInL(std::string);
bool isWord(std::string);
bool strIsInL(std::string);
using namespace std;
int main() {
	std::cout << "$" << endl << "$$" << endl << "$$$" << endl << "$$$$"
	<< endl << "$$$$$" << endl << "$$$$$$" << endl << "$$$$$$$" << endl
	<< "abb" << endl << "aabbbb" << endl << "$abb" << endl << "$$abb"
	<< endl << "$$$abb" << endl << "$$$$abb" << endl << "$aabbbb" << endl;
	cout << testIsWord("....---") << endl << testIsWord("-.-.-.") << endl;
	cout << testStrIsInL("ABB") << endl << testStrIsInL("AABBBB") << endl <<
			testStrIsInL("AABBBBA") << endl;
    return 0;
}
/**
 * This function is our test for isWord
 * @param s is the string to be tested.
 * @return Yay if it passes and No if it fails
 */
string testIsWord(string s) {
	if (isWord(s)) {
		return "Yay";
	} else {
		return "No";
	}
}
/**
 * This function is our test for strIsInL
 * @param s is the string to be tested.
 * @return Yay if it passes and No if it fails
 */
string testStrIsInL(string s) {
	if (strIsInL(s)) {
		return "Yay";
	} else {
		return "No";
	}
}
/**
 * This function is a solution to Exercise 6, Chapter 5.
 * @param word a potential word in the language described in Exercise 6
 * @return true if the string is in the given language, false otherwise.
 */
bool isWord(std::string word) {
	//Length of the string coming in
	int end = word.length();
	//result to return when done
	bool result = false;
	
	//if you reach the empty string it is a valid word
	if (word == "") {
		result = true;
	}
	//if you don't have an empty string test to make sure
	//the last character isn't a - as that would be invalid.
	else if (word.at(end - 1) == '-') {
		return false;
	}
	//if first character is vaild remove it and continue on
	else if (word.at(0) == '.' || word.at(0) == '-'){
		result = isWord(word.erase(0,1));
	}
	//else the word is invalid
	else {
		result = false;
	}
	return result;
}
/**
 * This function is a solution to Exercise 9, Chapter 5.
 * @param s a potential string in language L
 * @return true if the given string is valid in L, false otherwise
 */
 /*Global variables that keep count of the amount of As and Bs
	We were not entirely sure if we were allowed to add
	parameters to this function so that's why we did it 
	this way. Other wise we would have been passing the count
	recursivley.
 */
float countA = 0;
float countB = 0;
bool strIsInL(std::string s) {
    bool result = false;
	//If you reach the end of the string count how
	//many As and Bs you have. There should be half
	//the amount of As as Bs
	if (s == "" ) {
		if (countA == (countB/2)) {
			result = true;
			countA = 0;
			countB = 0;
		}
		//otherwise the word fails
		else {
			result = false;
			countA = 0;
			countB = 0;
		}
	}
	//if you have counted a B then you should not see
	//an A.
	else if (countB >= 1 and s.at(0) == 'A') {
		countA = 0;
		countB = 0;
		return false;
	}
	//if the first char is an A increase countA and continue
	else if (s.at(0) == 'A') {
		countA += 1;
		result = strIsInL(s.erase(0,1));
	}
	//if it B increase countB and continue
	else if (s.at(0) == 'B') {
		countB += 1;
		result = strIsInL(s.erase(0,1));
	}
	//otherwise the word fails
	else {
		result = false;
		countA = 0;
		countB = 0;
	}
	//reset the counts and return the result
	countA = 0;
	countB = 0;
    return result;
}